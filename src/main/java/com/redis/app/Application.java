package com.redis.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.concurrent.CountDownLatch;

@SpringBootApplication
@EnableSpringConfigured
@ComponentScan(basePackages = "com.redis.*")
public class Application implements EmbeddedServletContainerCustomizer {

	public static void main(String[] args) throws InterruptedException {
		ApplicationContext ctx =  SpringApplication.run(Application.class, args);

		//StringRedisTemplate template = ctx.getBean(StringRedisTemplate.class);
		//CountDownLatch latch = ctx.getBean(CountDownLatch.class);

		//template.convertAndSend("chat", "Hello from Redis!");

		//latch.await();

		//System.exit(0);

	}

	@Override
	public void customize(ConfigurableEmbeddedServletContainer container) {
		// TODO Auto-generated method stub
		container.setPort(8083);
	}
}
