package com.redis.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.CountDownLatch;

@Service
public class Sender {


    @Autowired
    private StringRedisTemplate template;
    @Autowired
    private CountDownLatch latch ;

    public void sendMsg(String channel, String message){

        template.convertAndSend(channel, message);
    }
}
