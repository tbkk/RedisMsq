package com.redis.message;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.CountDownLatch;

public class MsgTest extends BaseTest{
    @Autowired
    private Sender sender;

    @Autowired
    private Receiver receiver;

    @Autowired
    private CountDownLatch latch;
    @Test
    public void testSendAndRecv(){
        sender.sendMsg ("chat","hello river fan , you are a good boy ");
        System.out.println ("hello world");

        try {
            Thread.sleep (5000);
        } catch (InterruptedException e) {
            e.printStackTrace ();
        }
        System.out.println ("over");
    }
}
